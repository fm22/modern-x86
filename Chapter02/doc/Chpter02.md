# Chapter 2

> X86-64 Core Programming -Part 1

## Visual Studio 配置编程环境

1. 新建 Visual Studio 项目之后，在源文件中添加后缀名为 `asm` 的源代码文件

2. 在项目文件上右击选择<u>生成依赖项</u>→<u>生成自定义</u>

   ![生成自定义依赖项](./生成自定义依赖项.png)

3. 添加 MASM

   ![添加 MASM 选项](./添加MASM选项.png)

4. 在 ASM 源码文件上右键配置属性

   ![配置 ASM 文件属性](./配置ASM文件属性.png)

5. 将项类型改为 Microsoft Macro Assembler

   ![配置成 MASM 类型文件](./配置成MASM类型文件.png)

## 手工编译

在 VS 2019 中，可以按 <kbd>Ctrl+`</kbd>进入调试终端，默认的调试终端如果为 32  位，可以在 VS 的工具，选项，环境，Terminal 选项卡中添加一个终端，其他与 开发者 PowerShell 类似，但是参数栏需要改为

```powershell
-NoExit -Command "& { Import-Module $env:VSAPPIDDIR\..\Tools\Microsoft.VisualStudio.DevShell.dll}; Enter-VsDevShell -SkipAutomaticLocation -DevCmdArguments '-arch=x64 -no_logo' -SetDefaultWindowTitle -InstallPath $env:VSAPPIDDIR\..\..\
```

在调试终端中进入源码文件夹，输入如下命令可以进行手工编译和链接

```powershell
ml64 /c .\IntegerAddSub.asm
cl /c .\Main.cpp
link /subsystem:console .\Main.obj .\IntegerAddSub.obj
.\Main.exe
```

其中

- `cl` 是 C/C++ 编译器，`cl /?` 可以查看编译器版本及位数
  - `/c` 参数表示只编译不链接
- `ml64 `是64 位 MASM 编译器
  - `/c` 参数表示只编译不链接
- `link` 是链接程序
  - `/subsystem:console` 表示选用 console 子系统
