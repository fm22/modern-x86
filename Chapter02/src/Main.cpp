#include <iostream>

using namespace std;

extern "C" int IntegerAddSub_(int a, int b, int c, int d);

static void PrintResult(const char* msg, int a, int b, int c, int d, int result) {
	cout << msg << endl;
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "c = " << c << endl;
	cout << "d = " << d << endl;
	cout << "result = " << result << endl;
	cout << endl;
}

int main() {
	int a, b, c, d, result;

	a = 10; b = 20; c = 30; d = 18;
	result = IntegerAddSub_(a, b, c, d);
	PrintResult("Test", a, b, c, d, result);

	a = 101; b = 34; c = -190; d = 25;
	result = IntegerAddSub_(a, b, c, d);
	PrintResult("Test", a, b, c, d, result);
	return 0;
}